using Toybox.Application;
using Toybox.ActivityMonitor;

class StepsElement{
		
	var stepsView = null;
	    
	function initialize(){
	}
	
	function setupViews(element){
	    stepsView = element;
		stepsView.setColor(AppProperty.getValue("ActivityColor"));
	}
	
	function applySettings(){
		stepsView.setColor(AppProperty.getValue("ActivityColor"));
	}
		
	function update(){
	    stepsView.setText(ActivityMonitor.getInfo().steps.toString());	
	}
}