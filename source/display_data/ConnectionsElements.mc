using Toybox.System;

class ConnectionsElements{
		
	var connectionView = null;
	    
	function initialize(){
	}
	
	function setupViews(connectionElement){
		connectionView = connectionElement;
	    connectionView.setColor(AppProperty.getValue("ClockColor"));
	}
	
	function applySettings(){
		connectionView.setColor(AppProperty.getValue("ClockColor"));
	}
		
	function update(){
		setConnectedDisplay();
	}
	
	private function setConnectedDisplay(){
		if(System.getDeviceSettings().phoneConnected){
			connectionView.setText("C");
        } else{
        	connectionView.setText("");
        }         	
	}	
}