using Toybox.Time.Gregorian as Calendar;

class WeekDaysElement{
		
		var firstWeekDay = null;
		var secondWeekDay = null;
		var thirdWeekDay = null;
		var forthWeekDay = null;
		var fifthWeekDay = null;
		var sixWeekDay = null;
		var sevenWeekDay = null;
		
		var firstDayIsMondey = true;
		
		var nameList = [ "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" ];
		var elementList = null;
		var info = null;
		var dayIndex = 1;
		
	function initialize(){
	}
	
	function setupViews(firstWeekDay, secondWeekDay, thirdWeekDay, forthWeekDay, fifthWeekDay, sixWeekDay, sevenWeekDay ){
		elementList = [firstWeekDay, secondWeekDay, thirdWeekDay, forthWeekDay, fifthWeekDay, sixWeekDay, sevenWeekDay];
		firstDayIsMondey = AppProperty.getValue("IsMondayFirstDay");
		if(firstDayIsMondey){
			setText(nameList, elementList);
		} else{
			setText(setUpSundayFirstDay(), elementList);
		}
	}
	
	function setUpSundayFirstDay(){
		var tempList = new [7];
		tempList[0] = nameList[6];
		for (var i = 0; i < 6; i += 1){
			tempList[i+1] = nameList[i];
		}
		return tempList;
	}
	
	function setText(dayNameList, elementList){
		for (var i = 0; i < 7; i += 1){
			elementList[i].setText(dayNameList[i]);
			elementList[i].setColor(AppProperty.getValue("WeekDaysColor"));
		}
	}
	
	function applySettings(){
		firstDayIsMondey = AppProperty.getValue("IsMondayFirstDay");
		if(firstDayIsMondey){
			setText(nameList, elementList);
		} else{
			setText(setUpSundayFirstDay(), elementList);
		}
		elementList[dayIndex-1].setColor(AppProperty.getValue("CurrentDayColor"));	
	}
	
	function update(){
	    var info = Calendar.info( Time.now(), Calendar.FORMAT_SHORT );
		var updateDayIndex = null;
		if(firstDayIsMondey){
			updateDayIndex = info.day_of_week-1;
			if (updateDayIndex == 0){ updateDayIndex = 7; }
		} else {
			updateDayIndex = info.day_of_week;
		}
		if(dayIndex != updateDayIndex){
			for (var i = 0; i < 7; i += 1){
				elementList[i].setColor(AppProperty.getValue("WeekDaysColor"));
			}
			elementList[updateDayIndex-1].setColor(AppProperty.getValue("CurrentDayColor"));
			dayIndex = updateDayIndex;
		}
		
	}
}