using Toybox.Application;
using Toybox.Time.Gregorian as Calendar;

class DateElement{
		
	var dateView = null;
	    
	function initialize(){
	}
	
	function setupViews(element){
		dateView = element;
		dateView.setColor(AppProperty.getValue("DateColor"));		
	}
	
	function applySettings(){
		dateView.setColor(AppProperty.getValue("DateColor"));	
	}
		
	function update(){
    	var dateInfo = Calendar.info( Time.now(), Calendar.FORMAT_SHORT );
		var day = dateInfo.day;
		day = day.format("%02d");
		var month = dateInfo.month;
		month = month.format("%02d");
		var dateString = Lang.format("$1$.$2$.$3$",
         [ day, month, dateInfo.year ]);
		dateView.setText(dateString);	
	}
}