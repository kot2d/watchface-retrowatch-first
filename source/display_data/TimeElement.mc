using Toybox.Application;
using Toybox.System;

class TimeElement{
		
	var minuteView = null;
	var hourView = null;
	var dotsVeiw = null;
	    
	function initialize(){
	}
	
	function setupViews(hourElement, minuteElement, dotsElement){
		hourView = hourElement;
	    hourView.setColor(AppProperty.getValue("ClockColor"));
		minuteView = minuteElement;
        minuteView.setColor(AppProperty.getValue("ClockColor"));
        dotsVeiw = dotsElement;
        dotsVeiw.setColor(AppProperty.getValue("ClockColor"));
	}
	
	function applySettings(){
		hourView.setColor(AppProperty.getValue("ClockColor"));
		minuteView.setColor(AppProperty.getValue("ClockColor"));
		dotsVeiw.setColor(AppProperty.getValue("ClockColor"));
	}
		
	function update(){
        var clockTime = System.getClockTime();
        var hours = clockTime.hour;
        if (!System.getDeviceSettings().is24Hour) {
            if (hours > 12) {
                hours = hours - 12;
                hours = hours.format("%02d");
            }
        } else {
        	hours = hours.format("%02d");
        }
        hourView.setText(hours.toString());
        minuteView.setText(clockTime.min.format("%02d"));
	}
}