using Toybox.Application;
using Toybox.ActivityMonitor;

class CaloriesElement{
		
	var calView = null;
	    
	function initialize(){
	}
	
	function setupViews(element){
	    calView = element;
		calView.setColor(AppProperty.getValue("ActivityColor"));
	}
	
	function applySettings(){
		calView.setColor(AppProperty.getValue("ActivityColor"));
	}
		
	function update(){
	    calView.setText(ActivityMonitor.getInfo().calories.toString());	
	}
}