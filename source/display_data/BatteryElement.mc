using Toybox.Application;
using Toybox.System;
using AppProperty;

class BatteryElement{
		
	var batteryView = null;
	    
	function initialize(){
	}
	
	function setupViews(element){
		batteryView = element;
		batteryView.setColor(AppProperty.getValue("ActivityColor"));		
	}
	
	function applySettings(){
		batteryView.setColor(AppProperty.getValue("ActivityColor"));	
	}
		
	function update(){
		var myStats = System.getSystemStats();
		batteryView.setText(myStats.battery.format("%d"));
	}
}