using Toybox.Application;
using Toybox.ActivityMonitor;

class DistanceElement{
		
		var distance = null;
	    var distanceView = null;
	    var km_distance = null;
	    var textColor = null;
	    
	function initialize(){
	}
	
	function setupViews(element){
	    distanceView = element;
		distanceView.setColor(AppProperty.getValue("ActivityColor"));
	}
	
	function applySettings(){
		textColor = AppProperty.getValue("ActivityColor");
		distanceView.setColor(textColor);
	}
		
	function update(){
		distance = ActivityMonitor.getInfo().distance.toFloat();
		km_distance = (distance/100000).toFloat();
		km_distance = km_distance.format("%.2f");
	    distanceView.setText(km_distance.toString());		
	}
}