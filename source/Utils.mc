using Toybox.Application;

class AppProperty{
	
	function initialize(){
	}
	
	static function getValue(value){
	if(Application has :Storage) {return Application.Properties.getValue(value); }
	else { return Application.getApp().getProperty(value); }
	}
}