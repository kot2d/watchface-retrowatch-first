using Toybox.Graphics;
using Toybox.WatchUi as Ui;
using Toybox.System;


class BackgroundDrawable extends Ui.Drawable
{
	
	private var clockBackX = null;
    private var clockBackY = null;
	private var clockBackWidth = null;
	private var clockBackHeight = null;
	private var clockBackgroudColor = new AppProperty().getValue("ClockBacgroundColor");

	private var activityBackX = null;
    private var activityBackY = null;
	private var activityBackWidth = null;
	private var activityBackHeight = null;
	private var avtivityBackgroudColor = new AppProperty().getValue("ActivityBackgroundColor");

	private var is_changed = true;
	private var position_is_set_up = false;
	
	function initialize(options) {
			Drawable.initialize(options);
	}
	
	function draw(dc) {
		if(!position_is_set_up){
			setup_position(dc);
		}
		drawClockBack(dc);
		drawActivityBack(dc);
	}
	
	private function setup_position(dc){
		clockBackX = 0;
    	clockBackY = get_y_wyth_percent(dc, 28);
		clockBackWidth = get_x_wyth_percent(dc, 100);
		clockBackHeight = get_y_wyth_percent(dc, 30);
	
		activityBackX = 0;
		activityBackY = get_y_wyth_percent(dc, 60);
		activityBackWidth = get_x_wyth_percent(dc, 100);
		activityBackHeight = get_y_wyth_percent(dc, 30);
		position_is_set_up = true;
	}
	
	function apply_changes(){
		clockBackgroudColor = new AppProperty().getValue("ClockBacgroundColor");
		avtivityBackgroudColor = new AppProperty().getValue("ActivityBackgroundColor");
	}
	
	private function drawClockBack(dc) {
		dc.setColor(clockBackgroudColor, Graphics.COLOR_TRANSPARENT);
		dc.fillRectangle(clockBackX,clockBackY,clockBackWidth,clockBackHeight);
	}
	
	private function drawActivityBack(dc) {
		dc.setColor(avtivityBackgroudColor, Graphics.COLOR_TRANSPARENT);
		dc.fillRectangle(activityBackX,activityBackY,activityBackWidth,activityBackHeight);
	}	
	
	private function get_y_wyth_percent(dc, percent){
		return ((percent * dc.getHeight())/100).toNumber();
	}
	
	private function get_x_wyth_percent(dc, percent){
		return ((percent * dc.getWidth())/100).toNumber();
	}
}

