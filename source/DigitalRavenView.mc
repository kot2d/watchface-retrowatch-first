using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Application;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;


class DigitalRavenView extends WatchUi.WatchFace {
	
	var myBitmap;
    var backDrawable = null;
    var distanceElement = null;
    var daysElement = null;
    var caloriesElement = null;
    var stepsElement = null;
    var timeElement = null;
    var dateElement = null;
    var batteryElement = null;
    var connectionsElements = null;
    
    function initialize() {
        distanceElement = new DistanceElement();
		daysElement = new WeekDaysElement();
		caloriesElement = new CaloriesElement();
		stepsElement = new StepsElement();
		timeElement = new TimeElement();
		dateElement = new DateElement();
		batteryElement = new BatteryElement();
		connectionsElements = new ConnectionsElements();
    }

    function onLayout(dc) {
		setLayout(Rez.Layouts.WatchFace(dc));
		backDrawable = findDrawableById("BackDrawable");
		distanceElement.setupViews(findDrawableById("DistView"));
		caloriesElement.setupViews(findDrawableById("CalView"));
		stepsElement.setupViews(findDrawableById("StepsView"));
		timeElement.setupViews(findDrawableById("HourView"),
							   findDrawableById("MinuteView"),
							   findDrawableById("DotsView"));
		dateElement.setupViews(findDrawableById("DateView"));
		batteryElement.setupViews(findDrawableById("BatteryView"));
		daysElement.setupViews(View.findDrawableById("FirstWeekDay"),
									      View.findDrawableById("SecondWeekDay"),
									      View.findDrawableById("ThirdWeekDay"),
									      View.findDrawableById("ForthWeekDay"),
									      View.findDrawableById("FifthWeekDay"),
									      View.findDrawableById("SixWeekDay"),
									      View.findDrawableById("SevenWeekDay"));
		connectionsElements.setupViews(View.findDrawableById("ConnectionView"));									      
    }
	
	function set_changes(){
		backDrawable.apply_changes();
		distanceElement.applySettings();
		daysElement.applySettings();
		caloriesElement.applySettings();
		stepsElement.applySettings();
		timeElement.applySettings();
		dateElement.applySettings();
		batteryElement.applySettings();
		connectionsElements.applySettings();
	}
	
    function onUpdate(dc) {
		daysElement.update();
        dateElement.update();
		timeElement.update();        
    	distanceElement.update();
    	caloriesElement.update();
    	stepsElement.update();
    	batteryElement.update();	
        connectionsElements.update();
        View.onUpdate(dc);
    }
	
    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }

}
